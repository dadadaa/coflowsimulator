package coflowSim;

import coflowSim.simulators.FairSimulator;
import coflowSim.simulators.Simulator;
import coflowSim.traceproducers.CoflowBenchmarkTraceProducer;
import coflowSim.traceproducers.TestTraceProducer;
import coflowSim.traceproducers.TraceProducer;
import coflowSim.utils.Constants;

import static coflowSim.utils.Constants.RACK_BYTES_PER_SEC;
import static coflowSim.utils.Constants.SIMULATION_SECOND_MILLS;

/**
 * Created by da on 26/3/2017.
 */
public class CoflowSim {

    public static void main(String[] args){
        int curArg = 0;

        //Default scheduing algorithm is DRF
        Constants.SCHEDULING_ALGO schedulingAlgo = Constants.SCHEDULING_ALGO.DRF;
        if (args.length > curArg){
            String UPPER_ARG = args[curArg++].toUpperCase();

            if (UPPER_ARG.contains("DRF")){
                schedulingAlgo = Constants.SCHEDULING_ALGO.DRF;
            } else if (UPPER_ARG.contains("DRFEDF")){
                schedulingAlgo = Constants.SCHEDULING_ALGO.DRFEDF;
            } else if (UPPER_ARG.contains("VARYS")){
                schedulingAlgo = Constants.SCHEDULING_ALGO.VARYS;
            } else if (UPPER_ARG.contains("HUG")){
                schedulingAlgo = Constants.SCHEDULING_ALGO.HUG;
            } else {
                System.err.println("Unsupported or wrong scheduling algorithm");
                System.exit(1);
            }
        }

        TraceProducer traceProducer = null;
        Constants.TRACE_FORMAT traceFormat = Constants.TRACE_FORMAT.DEFAULT;
        if (args.length > curArg){
            String UPPER_ARG = args[curArg++].toUpperCase();

            if (UPPER_ARG.contains("DEFAULT")){
                traceFormat = Constants.TRACE_FORMAT.DEFAULT;
            } else if (UPPER_ARG.contains("STARTZERO")){
                traceFormat = Constants.TRACE_FORMAT.START_ZERO;
            } else if (UPPER_ARG.contains("TEST")) {
                traceProducer = new TestTraceProducer(Constants.TRACE_PRODUCER_ALGO.ALL_TO_ALL_SIZE_DIFF,
                        3, 5, RACK_BYTES_PER_SEC);
            }else {
                System.err.println("Unsupported or wrong trace format");
                System.exit(1);
            }
        }


        if (args.length > curArg){
            String pathToCoflowBenchmarkTraceFile = args[curArg++];
            traceProducer = new CoflowBenchmarkTraceProducer(pathToCoflowBenchmarkTraceFile);
        }

        Simulator coflowSimulator = null;

        if (schedulingAlgo == Constants.SCHEDULING_ALGO.DRF){
            coflowSimulator = new FairSimulator(Constants.SCHEDULING_ALGO.DRF, traceProducer);
        }

        coflowSimulator.simulate(10 * SIMULATION_SECOND_MILLS);
    }
}
