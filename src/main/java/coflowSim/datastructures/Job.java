package coflowSim.datastructures;

import java.util.ArrayList;

/**
 * Created by da on 26/3/2017.
 */
public class Job {
    public ArrayList<Flow> allFlows = new ArrayList<>();
    public ArrayList<Flow> activeFlows;
    public boolean isFinished;
    public long arrivalTime;
    public long startTime;
    public long finishTime = 0;
    public int id;
    public boolean isAdmitted = false;

    public Job(int arrivalTime, int id){
        this.arrivalTime = arrivalTime;
        this.id = id;
        activeFlows = new ArrayList<>();
        isFinished = false;
    }

    /**
     * Add flows from trace producer.
     * At the very beginning, every flow is an active flow.
     */
    public void addFlows(Flow newFlow){
        activeFlows.add(newFlow);
        allFlows.add(newFlow);
    }

}
