package coflowSim.datastructures;

/**
 * Created by da on 26/3/2017.
 */
public class Flow {
    private int id;

    public final int sourceId;
    public final int destinationId;
    private final double totalBytes;

    public double bytesRemaining;
    public double currentBps = 0.0;
    public boolean isFinished;

    public Flow(int src, int dst, double totalBytes){
        this.sourceId = src;
        this.destinationId = dst;
        this.totalBytes = totalBytes;
        this.isFinished = false;

        this.bytesRemaining = this.totalBytes;
    }



}
