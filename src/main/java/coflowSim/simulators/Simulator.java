package coflowSim.simulators;

import coflowSim.datastructures.Flow;
import coflowSim.datastructures.Job;
import coflowSim.traceproducers.TraceProducer;
import coflowSim.utils.Constants;

import java.io.FileNotFoundException;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.HashSet;

import static coflowSim.utils.Constants.RACK_BITS_PER_SEC;
import static coflowSim.utils.Constants.SIMULATION_QUANTA;
import static coflowSim.utils.Constants.SIMULATION_SECOND_MILLS;

/**
 * Created by da on 27/3/2017.
 */
public class Simulator {
    public int NUM_RACKS;
    public int NUM_JOBS;

    protected HashMap<Integer, Job> activeJobs = new HashMap<>();
    protected HashMap<Integer, Job> allJobs;

    protected Constants.SCHEDULING_ALGO schedulingAlgo;

    protected long CURRENT_TIME = 0;

    //Free Band Width
    double[] freeBps;

    public Simulator(
            Constants.SCHEDULING_ALGO schedulingAlgo,
            TraceProducer traceProducer
    ){
        NUM_RACKS = traceProducer.getNumRacks();
        NUM_JOBS = traceProducer.getNumJobs();

        this.schedulingAlgo = schedulingAlgo;

        this.allJobs = traceProducer.jobs;

        assert NUM_JOBS == allJobs.size();

        freeBps = new double[2*NUM_RACKS];

        Arrays.fill(freeBps, RACK_BITS_PER_SEC);
    }

    //To minimize the simulation gap, the epoch in mills is highly recommanded to
    public void simulate(long EPOCH_IN_MILLS){
        int curJob = 1;
        System.out.printf("Start simulate\n");
        System.out.printf("Size of jobs: %d.\n", allJobs.size());

        //Main loop
        for (CURRENT_TIME = 0; (curJob <= NUM_JOBS || activeJobs.size() > 0 ); CURRENT_TIME += EPOCH_IN_MILLS){

            int jobsAdded = 0;

            //Increase job index
            for (; curJob <= NUM_JOBS; ++curJob){
                Job j = allJobs.get(curJob);

                if (j.arrivalTime > CURRENT_TIME + EPOCH_IN_MILLS){
                    break;
                }

                j.isAdmitted = true;
                jobsAdded++;
                uponJobAdmission(j);
            }

            //Stuff to do on new job arrival, that is, to update rate
            if (jobsAdded > 0){
                afterJobAdmission(CURRENT_TIME);
            }

            //SIMULATION_QUANTA is time step of the simulator
            for (long i = 0; i < EPOCH_IN_MILLS; i += SIMULATION_QUANTA){
                int numActiveJobs = activeJobs.size();
                if (numActiveJobs == 0){
                    break;
                }

                long curTime = CURRENT_TIME + i;

                //For debugging
//                double minFree = RACK_BITS_PER_SEC;
//                for (int j = 0; j < 2 * NUM_RACKS; j++) {
//                    if (freeBps[j] < minFree){
//                        minFree = freeBps[j];
//                    }
//                }
//
//                System.out.println("Current minimum free bandwidth is " + minFree);

                onSchedule(curTime);

                //Print progress
                if (curTime % SIMULATION_SECOND_MILLS == 0){
                    System.out.printf("Timestep: %6d Running: %3d Started %5d\n",
                            (curTime)/SIMULATION_SECOND_MILLS, numActiveJobs, curJob - 1);

                    //Print bottle neck port free bandwidth to test if resources are fully utilized


                }

                //Stuff after job departures
                if (numActiveJobs > activeJobs.size()){
                    afterJobDeparture(curTime);
                }

            }

        }

        System.out.println("Reach here.");

        logResults();
    }

    //For each flow in each active job, remaining bytes -= bw * time step.
    //To free allocated bandwidth after a flow has finished
    public void onSchedule(long curTime){
        HashSet<Integer> jobToBeRemoveID = new HashSet<>();
        for (Job j :
                activeJobs.values()) {
            ArrayList<Flow> flowsToBeRemove = new ArrayList<>();
            for (Flow f :
                    j.activeFlows) {
                f.bytesRemaining -= f.currentBps / 8.0 * SIMULATION_QUANTA / SIMULATION_SECOND_MILLS;
                if (f.bytesRemaining <= 0.0){
                    f.isFinished = true;
                    //Unchecked
                    //Use removeall rather than remove
                    flowsToBeRemove.add(f);
                    //j.activeFlows.remove(f);
                    freeBps[f.sourceId] += f.currentBps;
                    freeBps[f.destinationId + NUM_RACKS] += f.currentBps;
                }

            }
            j.activeFlows.removeAll(flowsToBeRemove);
            //This job already done.

            if (j.activeFlows.isEmpty()){
                j.finishTime = curTime + SIMULATION_QUANTA;
                //activeJobs.remove(j.id);
                jobToBeRemoveID.add(j.id);
            }

        }
        activeJobs.keySet().removeAll(jobToBeRemoveID);
    }

    public void afterJobAdmission(long currentTime){
        updateRates();
    }

    public void afterJobDeparture(long currentTime){
        updateRates();
    }

    /**
     * This method update currentBps for all active flows
     */
    public void updateRates(){};

    /**
     * Record start time and add the job to activeJobs
     * Maybe it doesn't need to be an abstract method
     * @param j
     */
    public void uponJobAdmission(Job j){
        activeJobs.put(j.id, j);
        j.startTime = CURRENT_TIME;
    }

    public void logResults(){
        String logPath = "log/"+schedulingAlgo+".txt";

        PrintWriter writer = null;
        try {
            writer = new PrintWriter(logPath, "UTF-8");
        } catch (FileNotFoundException | UnsupportedEncodingException e){
            e.printStackTrace();
            System.exit(1);
        }

        for (Job j : allJobs.values()){
            assert j.isFinished;
            long jobDurationMills = (j.finishTime - j.startTime) ;
            writer.println(j.id + " " + j.arrivalTime + " " + j.startTime + " "
            + j.finishTime + " " + jobDurationMills);
        }
        writer.close();
    }


}
