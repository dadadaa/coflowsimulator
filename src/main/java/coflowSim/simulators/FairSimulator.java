package coflowSim.simulators;

import coflowSim.datastructures.Flow;
import coflowSim.datastructures.Job;
import coflowSim.traceproducers.TraceProducer;
import coflowSim.utils.Constants;

import java.util.Arrays;
import java.util.HashMap;

import static coflowSim.utils.Constants.RACK_BITS_PER_SEC;

/**
 * Created by da on 27/3/2017.
 */
public class FairSimulator extends Simulator {

    HashMap<Integer, double[]> demandBytesByRack;
    HashMap<Integer, double[]> normalizedDemandByRack ;
    HashMap<Integer, Double> maxDemand ;
    double[] sumNormalizedDemand ;
    double progressBps = 0.0;

    public FairSimulator(
            Constants.SCHEDULING_ALGO schedulingAlgo,
            TraceProducer traceProducer
    ){
        super(schedulingAlgo, traceProducer);
    }

    @Override
    public void updateRates(){
        //However, it seems not necessary to update free bps.
        initialize();
        updateDemand();
        normalizeDemand();
        calculatePrgress();
        //However, this seems not necessary.
        //updateAllocation();
        updateFlowRate();

        //Print rates, for debugging
        //printRates();
    }

    private void initialize(){
        Arrays.fill(freeBps, RACK_BITS_PER_SEC);
        sumNormalizedDemand = new double[2 * NUM_RACKS];
        demandBytesByRack = new HashMap<>();
        normalizedDemandByRack = new HashMap<>();
        maxDemand = new HashMap<>();
    }

    private void printRates(){
        for (Job j :
                activeJobs.values()) {
            for (Flow f:
                 j.activeFlows) {
                System.out.printf("Job ID: %d,\t Source port: %d,\t Destination port: %d,\t ",
                        j.id, f.sourceId, f.destinationId);
                //The problem is that current Bps is too large.
                System.out.printf("Remaining bytes: %f, \t Current Bps: %f\n", f.bytesRemaining,
                        f.currentBps);
            }
        }
    }

    private void updateDemand(){
        for (Job j :
                activeJobs.values()) {
            double[] currentDemandBytes = new double[2 * NUM_RACKS];
            Arrays.fill(currentDemandBytes, 0.0);
            for (Flow f :
                    j.activeFlows) {
                currentDemandBytes[f.sourceId] += f.bytesRemaining;
                currentDemandBytes[f.destinationId + NUM_RACKS] += f.bytesRemaining;
            }
            demandBytesByRack.put(j.id, currentDemandBytes);
        }
    }

    private void normalizeDemand(){
        for (Integer jobID :
                demandBytesByRack.keySet()) {
            double[] currentDemandBytes = demandBytesByRack.get(jobID);
            //normalizedDemandBytes
            double[] currentNormalizedDemandBytes = new double[2 * NUM_RACKS];
            Arrays.fill(currentNormalizedDemandBytes, 0.0);

            double max = 0.0;
            for (int i = 0; i < currentDemandBytes.length; i++) {
                if (currentDemandBytes[i] > max){
                    max = currentDemandBytes[i];
                }
            }

            maxDemand.put(jobID, max);

            for (int i = 0; i < currentDemandBytes.length; i++) {
                currentNormalizedDemandBytes[i] = currentDemandBytes[i] / max;
            }

            //Test if the max normalized demand is 1.0
//            double maxNor = 0.0;
//            for (int i = 0; i < 2 * NUM_RACKS; i++) {
//                if (currentNormalizedDemandBytes[i] > maxNor){
//                    maxNor = currentNormalizedDemandBytes[i];
//                }
//            }
//
//            System.out.println("active Job size " + demandBytesByRack.size());

            normalizedDemandByRack.put(jobID, currentNormalizedDemandBytes);

        }
    }

    private void calculatePrgress(){
        Arrays.fill(sumNormalizedDemand, 0.0);
        for (Integer jobID :
                demandBytesByRack.keySet()) {
            for (int i = 0; i < 2 * NUM_RACKS; i++) {
                sumNormalizedDemand[i] += normalizedDemandByRack.get(jobID)[i];
            }
        }

        double maxNormalizedDemand = 0.0;

        for (int i = 0; i < 2 * NUM_RACKS; i++) {
            if (maxNormalizedDemand < sumNormalizedDemand[i]){
                maxNormalizedDemand = sumNormalizedDemand[i];
            }
        }

        //This is the bandwidth allocation of the bottleneck port of each job
        //(Maybe the work bottleneck is not correct)
        progressBps = RACK_BITS_PER_SEC / maxNormalizedDemand;
    }

    //Unchecked
    private void updateFlowRate(){
        for (Job j :
                activeJobs.values()) {
            double progressTimeSecond = maxDemand.get(j.id) * 8.0 / progressBps;
            //System.out.println("Job ID: " + j.id + "prgress Time in second: " + progressTimeSecond);
            for (Flow f :
                    j.activeFlows) {
                f.currentBps = f.bytesRemaining * 8.0 / progressTimeSecond;
                freeBps[f.sourceId] -= f.currentBps;
                freeBps[f.destinationId + NUM_RACKS] -= f.currentBps;
            }
        }
    }

}
