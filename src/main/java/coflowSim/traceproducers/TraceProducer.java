package coflowSim.traceproducers;

import coflowSim.datastructures.Job;

import java.util.ArrayList;
import java.util.HashMap;

/**
 * Created by da on 26/3/2017.
 */
public abstract class TraceProducer {
    public HashMap<Integer, Job> jobs;
    public int NUM_RACKS;
    public int NUM_JOBS;

    public TraceProducer(){
        jobs = new HashMap<>();
    }

    public abstract void prepareTrace();

    public int getNumRacks(){
        return NUM_RACKS;
    }

    public int getNumJobs(){
        return NUM_JOBS;
    }

}
