package coflowSim.traceproducers;

import coflowSim.datastructures.Flow;
import coflowSim.datastructures.Job;
import coflowSim.utils.Constants;

/**
 * Created by da on 30/3/2017.
 */
public class TestTraceProducer extends TraceProducer{
    private Constants.TRACE_PRODUCER_ALGO traceProducerAlgo = Constants.TRACE_PRODUCER_ALGO.ALL_TO_ALL;
    private double perFlowSizeBytes;

    public TestTraceProducer(Constants.TRACE_PRODUCER_ALGO traceProducerAlgo, int numRacks,
                             int numJobs, double perFlowSizeBytes){
        this.traceProducerAlgo = traceProducerAlgo;
        this.perFlowSizeBytes = perFlowSizeBytes;
        NUM_JOBS = numJobs;
        NUM_RACKS = numRacks;

        prepareTrace();
    }

    @Override
    public void prepareTrace(){
        switch (traceProducerAlgo) {
            case ALL_TO_ALL :
                prepareTraceAllToAll();
                break;
            case ONE_TO_ONE :
                prepareTraceOneToOne();
                break;
            case ALL_TO_ALL_SIZE_DIFF:
                prepareTraceAllToAllSizeDiff();
                break;
        }
    }

    private void prepareTraceAllToAll(){
        for (int i = 1; i <= NUM_JOBS; i++) {
            Job currentJob = new Job(0, i);
            for (int j = 0; j < NUM_RACKS; j++) {
                for (int k = 0; k < NUM_RACKS; k++) {
                    Flow currentFlow = new Flow(k, j, perFlowSizeBytes);
                    currentJob.addFlows(currentFlow);
                }
            }
            jobs.put(i, currentJob);
        }
    }

    private void prepareTraceOneToOne(){
        for (int i = 1; i <= NUM_JOBS; i++) {
            Job currentJob = new Job(0, i);
            for (int j = 0; j < NUM_RACKS; j++) {
                Flow currentFlow = new Flow(j, j, perFlowSizeBytes);
                currentJob.addFlows(currentFlow);
            }
            jobs.put(i, currentJob);
        }
    }

    private void prepareTraceAllToAllSizeDiff(){
        for (int i = 1; i <= NUM_JOBS; i++) {
            Job currentJob = new Job(0, i);
            for (int j = 0; j < NUM_RACKS; j++) {
                for (int k = 0; k < NUM_RACKS; k++) {
                    Flow currentFlow = new Flow(k, j, i * perFlowSizeBytes);
                    currentJob.addFlows(currentFlow);
                }
            }
            jobs.put(i, currentJob);
        }
    }
}
