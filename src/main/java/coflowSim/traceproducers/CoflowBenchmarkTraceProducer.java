package coflowSim.traceproducers;

import coflowSim.datastructures.Flow;
import coflowSim.datastructures.Job;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;

/**
 * Created by da on 26/3/2017.
 */
public class CoflowBenchmarkTraceProducer extends TraceProducer {

    private String pathToCoflowBenchmarkTraceFile;

    public CoflowBenchmarkTraceProducer(String pathToCoflowBenchmarkTraceFile){
        this.pathToCoflowBenchmarkTraceFile = pathToCoflowBenchmarkTraceFile;
        prepareTrace();
        //For debugging
        //printJobs();
    }

    @Override
    public void prepareTrace(){
        FileReader fileReader = null;
        try {
            fileReader = new FileReader(pathToCoflowBenchmarkTraceFile);
        } catch (FileNotFoundException e){
            System.err.println("Couldn't open" + pathToCoflowBenchmarkTraceFile);
            System.exit(1);
        }

        //Read number of racks and number of jobs from the trace
        BufferedReader br = new BufferedReader(fileReader);
        try {
            String line = br.readLine();
            String[] splits = line.split("\\s+");

            NUM_RACKS = Integer.parseInt(splits[0]);
            NUM_JOBS = Integer.parseInt(splits[1]);
        } catch (IOException e){
            System.err.println("Missing trace description in "+ pathToCoflowBenchmarkTraceFile);
            System.exit(1);
        }

        //Read trace of each job
        for (int i = 0; i < NUM_JOBS; i++) {
            try {
                String line = br.readLine();
                String[] splits = line.split("\\s+");
                int lIndex = 0;

                int jobID = Integer.parseInt(splits[lIndex++]);
                int jobArrivialTime = Integer.parseInt(splits[lIndex++]);

                Job currentJob = new Job(jobArrivialTime, jobID);

                int numMappers = Integer.parseInt(splits[lIndex++]);
                ArrayList<Integer> mapperID = new ArrayList<>();
                for (int mID = 0; mID < numMappers; mID++) {
                    mapperID.add(Integer.parseInt(splits[lIndex++]));
                }

                int numReducers = Integer.parseInt(splits[lIndex++]);
                for (int rID = 0; rID < numReducers; rID++) {

                    //1 <= rackIndex <= NUM_RACKS
                    String rack_MB = splits[lIndex++];

                    int rackIndex = Integer.parseInt(rack_MB.split(":")[0]);
                    double totalBytes = Double.parseDouble(rack_MB.split(":")[1]) * 1048576.0;

                    double bytesPerFlow = totalBytes / numMappers;

                    for (int currentMapper :
                            mapperID) {
                        Flow currentFlow = new Flow(currentMapper, rackIndex, bytesPerFlow);
                        currentJob.addFlows(currentFlow);
                    }
                }

                jobs.put(jobID, currentJob);

            } catch (IOException e){
                System.err.println("Missing job in " + pathToCoflowBenchmarkTraceFile);
                System.exit(1);
            }
        }
    }

//    @Override
//    public int getNumRacks(){
//        return NUM_RACKS;
//    }
//
//    @Override
//    public int getNumJobs(){
//        return NUM_JOBS;
//    }

    /**
     * For debugging
     */
    private void printJobs(){
        for (Job j :
                jobs.values()) {
            System.out.println("Job ID: "+ j.id + " Size of active flows: " + j.activeFlows.size());
            for (Flow f :
                    j.activeFlows) {
                System.out.println("Job ID: " + j.id + " FLow source: " + f.sourceId +
                " Flow destination: " + f.destinationId + " Flow size: " + f.bytesRemaining);
            }
        }
    }
}
