package coflowSim.utils;

/**
 * Created by da on 26/3/2017.
 */
public class Constants {
    public enum SCHEDULING_ALGO {
        DRF,
        DRFEDF,
        VARYS,
        HUG,
    }

    public enum TRACE_FORMAT {
        START_ZERO,
        DEFAULT,
        TEST,
    }

    public enum TRACE_PRODUCER_ALGO {
        ALL_TO_ALL,
        ONE_TO_ONE,
        ALL_TO_ALL_TIME_DIFF,
        ALL_TO_ALL_SIZE_DIFF,
        ALL_TO_ALL_TIME_SIZE_DIFF,
    }

    public static final double RACK_BITS_PER_SEC = 1.0 * 1024 * 1048576;

    public static final double RACK_BYTES_PER_SEC = RACK_BITS_PER_SEC / 8;

    public static final int SIMULATION_SECOND_MILLS = 1024;

    /**
     * Time step of {@link coflowSim.simulators.Simulator#simulate(long)}
     *
     * This is 8.
     */
    public static final int SIMULATION_QUANTA = SIMULATION_SECOND_MILLS
            / (int) (RACK_BYTES_PER_SEC / 1048576);
}
